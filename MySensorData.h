#ifndef MYSENSORDATA_H
#define MYSENSORDATA_H

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

string error;

struct data_sensor
{
    float kHeading = 0;  // 05
    float kPitch = 0; // 24
    float kRoll = 0; // 25
    char kHeadingStatus = '0'; // 79
    float kQuaternion[4] = {0,0,0,0}; // 77
    float kTemperature = 0; // 07
    bool kDistartion = true; // 08
    bool CalStatus = true; // 09
    float kAccelX = 0; // 21
    float kAccelY = 0; // 22
    float kAccelZ = 0; // 23
    float kMagX = 0; // 27
    float kMagY = 0; // 28
    float kMagZ = 0; // 29
    float kGyroX = 0; // 74
    float kGyroY = 0; // 75
    float kGyroZ = 0; // 76

    void print_data()
    {
        if (error != "OK") {cout << error << endl;}
        else
        {
            cout << "kHeading = " << kHeading << " deg.\n";
            cout << "kPitch = " << kPitch << " deg.\n";
            cout << "kRoll = " << kRoll << " deg.\n";
            cout << "kHeadingStatus = " << kHeadingStatus << "\n";
            cout << "kQuaternion = " << kQuaternion[0] << " + " << kQuaternion[1] << "i + " << kQuaternion[2] << "j + " << kQuaternion[3] << "k\n";
            cout << "kTemperature = " << kTemperature << " deg.C\n";
            cout << boolalpha << "kDistartion = " << kDistartion << "\n";
            cout << "CalStatus = " << CalStatus << "\n";
            cout << dec << "kAccelX = " << kAccelX << " g\n";
            cout << "kAccelY = " << kAccelY << " g\n";
            cout << "kAccelZ = " << kAccelZ << " g\n";
            cout << "kMagX = " << kMagX << " mT\n";
            cout << "kMagY = " << kMagY << " mT\n";
            cout << "kMagZ = " << kMagZ << " mT\n";
            cout << "kGyroX = " << kGyroX << " rad/sec\n";
            cout << "kGyroY = " << kGyroY << " rad/sec\n";
            cout << "kGyroZ = " << kGyroZ << " rad/sec\n";
        }
    }
};

int BigToLittleInt( char big[8]) // перевод данных в int
{
    int result;
    sscanf(big, "%x", &result); // читает из big в формате шестнадцатиричного числа и записывает по адресу result
    return result;
}

data_sensor get_data(string read_data) // заполнение структуры данных датчика по строке посылке
{
    // read_data содержит посылку данных
    error = "OK"; // нужна для отслеживания ошибок
    data_sensor data; // будет хранить полученные данные
    char temp_8[8]; // нужна для выделения данных из посылки и передачи в функции перевода
    int ID, i=0;
    int temp_int; // нужна для получения данных из функции перевода для дальнейшего преобразованию к нужному типу
    while (read_data.size() > i) // strlen и sizeof выдают что-то странное
    {
        if (read_data.size()-i < 2) {error="data loss\n"; break;} // проверка возможности считать ID
        for (int j=0;j<6;j++)  // чтение данных для ID по формату для int
        {
            temp_8[j]='0';
        }
        temp_8[6]=read_data[i];
        temp_8[7]=read_data[i+1];
        i+=2;
        ID=BigToLittleInt(temp_8); // получение ID
        switch(ID)
                {
                case 5:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i+j];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kHeading=*((float*)&temp_int); // указатель на int преобразуется в указатель на float и берутся данные типа float по этому указателю
                        break;
                    }
                case 24:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kPitch=*((float*)&temp_int);
                        break;
                    }
                case 25:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kRoll=*((float*)&temp_int);
                        break;
                    }
                case 79:
                    {
                        if (read_data.size()-i < 2) {error="data loss\n"; break;}
                        for (int j=0;j<6;j++)  // чтение данных по формату для int
                        {
                            temp_8[j]='0';
                        }
                        temp_8[6]=read_data[i];
                        temp_8[7]=read_data[i+1];
                        i+=2;
                        temp_int=BigToLittleInt(temp_8);
                        data.kHeadingStatus=(char)temp_int;
                        break;
                    }
                case 77:
                    {
                        if (read_data.size()-i < 32) {error="data loss\n"; break;}
                        for (int k=0;k<4;k++)
                        {
                            for (int j=0;j<8;j++)
                            {
                                  temp_8[j]=read_data[i];
                            }
                            i+=8;
                            temp_int=BigToLittleInt(temp_8);
                            data.kQuaternion[k]=*((float*)&temp_int);
                        }
                        break;
                    }
                case 7:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kTemperature=*((float*)&temp_int);
                        break;
                    }
                case 8:
                    {
                        if (read_data.size()-i < 2) {error="data loss\n"; break;}
                        for (int j=0;j<6;j++)
                        {
                            temp_8[j]='0';
                        }
                        temp_8[6]=read_data[i];
                        temp_8[7]=read_data[i+1];
                        i+=2;
                        temp_int=BigToLittleInt(temp_8);
                        data.kDistartion=(bool)temp_int;
                        break;
                    }
                case 9:
                    {
                        if (read_data.size()-i < 2) {error="data loss\n"; break;}
                        for (int j=0;j<6;j++)
                        {
                            temp_8[j]='0';
                        }
                        temp_8[6]=read_data[i];
                        temp_8[7]=read_data[i+1];
                        i+=2;
                        temp_int=BigToLittleInt(temp_8);
                        data.CalStatus=(bool)temp_int;
                        break;
                    }
                case 21:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kAccelX=*((float*)&temp_int);
                        break;
                    }
                case 22:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kAccelY=*((float*)&temp_int);
                        break;
                    }
                case 23:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kAccelZ=*((float*)&temp_int);
                        break;
                    }
                case 27:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kMagX=*((float*)&temp_int);
                        break;
                    }
                case 28:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kMagY=*((float*)&temp_int);
                        break;
                    }
                case 29:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kMagZ=*((float*)&temp_int);
                        break;
                    }
                case 74:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kGyroX=*((float*)&temp_int);
                        break;
                    }
                case 75:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kGyroY=*((float*)&temp_int);
                        break;
                    }
                case 76:
                    {
                        if (read_data.size()-i < 8) {error="data loss\n"; break;}
                        for (int j=0;j<8;j++)
                        {
                            temp_8[j]=read_data[i];
                        }
                        i+=8;
                        temp_int=BigToLittleInt(temp_8);
                        data.kGyroZ=*((float*)&temp_int);
                        break;
                    }
                default:
                    {
                        error = "Wrong ID ";
                    }
                }
        if (error != "OK") break;
    }
    return data;
}

#endif // MYSENSORDATA_H
