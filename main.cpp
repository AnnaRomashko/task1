#include <iostream>
#include "MySensorData.h"

using namespace std;

int main()
{
    data_sensor data; // будет хранить полученные данные
    string read_data; // нужна для чтения посылки данных
    getline(cin,read_data); // получение посылки
    data=get_data(read_data); // чтение данных датчика
    data.print_data(); // вывод прочитанных данных
    return 0;
}
// 05430D083B183F91BCD8193EDF4DE94F00080015BCA408C0163BFB4DAB173F80F5081BC16EA4371cc12d31271d42513d4f4a000000004b000000004c00000000 - пример входных данных из презентации
